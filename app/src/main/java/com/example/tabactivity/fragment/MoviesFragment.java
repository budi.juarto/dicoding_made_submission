package com.example.tabactivity.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tabactivity.AnimeTraining;
import com.example.tabactivity.MyAsyncTaskLoader;
import com.example.tabactivity.R;
import com.example.tabactivity.adapter.TrainingRecViewAdapter;

import java.util.ArrayList;

//import com.example.tabactivity.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoviesFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<AnimeTraining>> {

    View v, w;

    private RecyclerView recyclerView;

    ProgressBar progressBar;
    private TrainingRecViewAdapter adapter;

//    private ArrayList<AnimeTraining> lstAnime = new ArrayList<AnimeTraining>();

//    public MoviesFragment() {
//
//    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_movies, container, false);
//        lstAnime = loadInBackground();
        recyclerView = v.findViewById(R.id.recyclerView);

        progressBar = v.findViewById(R.id.progress_bar);


        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        adapter = new TrainingRecViewAdapter(this.getContext());
        recyclerView.setAdapter(adapter);

        adapter.notifyDataSetChanged();
//        adapter.setTrainings(lstAnime);
//        Log.d("LIST:", lstAnime);
        Bundle bundle = new Bundle();

        progressBar.setVisibility(View.VISIBLE);
        getLoaderManager().initLoader(0, bundle, this);

        return v;


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @NonNull
    @Override
    public Loader<ArrayList<AnimeTraining>> onCreateLoader(int id, @Nullable Bundle args) {
        return new MyAsyncTaskLoader(getContext(),1);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<ArrayList<AnimeTraining>> loader, ArrayList<AnimeTraining> data) {
        adapter.setTrainings(data);

        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ArrayList<AnimeTraining>> loader) {
        adapter.setTrainings(null);
    }

//    public ArrayList<AnimeTraining> loadInBackground() {
//        SyncHttpClient client = new SyncHttpClient();
//        final ArrayList<AnimeTraining> animeTrainings = new ArrayList<>();
//        String url = BuildConfig.BASE_URL_MOVIE + BuildConfig.TMDB_API_KEY;
//        Log.d("URL",url);
//        client.get(url, new AsyncHttpResponseHandler() {
//
//            @Override
//            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
//                try {
//                    String result = new String(responseBody);
//                    Log.d("API RESULT", result);
//                    JSONObject responseObject = new JSONObject(result);
//                    JSONArray list = responseObject.getJSONArray("results");
//                    for (int i = 0; i < list.length(); i++) {
//                        JSONObject anime = list.getJSONObject(i);
//                        AnimeTraining animeTraining = new AnimeTraining(anime,1);
//                        animeTrainings.add(animeTraining);
//                    }
//                } catch (Exception e) {
//                    //Jika terjadi error pada saat parsing maka akan masuk ke catch()
//                    e.printStackTrace();
//
//                    Log.d("API :", "FAILED");
//                }
//            }
//
//            @Override
//            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
//
//                Log.d("API ]", "FAIL");
//            }
//        });
//        return animeTrainings;
//    }

}
