package com.example.tabactivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

public class TrainingActivity extends AppCompatActivity {
    private static final String TAG = "TrainingActivity";

    private TextView trainingName, trainingLongDesc;
    private ImageView trainingImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training);

        initViews();

        Intent intent = getIntent();
        try{
            AnimeTraining incomingTraining = intent.getParcelableExtra("training");
            trainingName.setText((incomingTraining.getName()));
            trainingLongDesc.setText(incomingTraining.getOverview());
            Glide.with(this)
                    .asBitmap()
                    .load("https://image.tmdb.org/t/p/w185" + incomingTraining.getPoster())
                    .into(trainingImage);


        }catch (NullPointerException e){
            e.getMessage();
        }

    }
    private  void initViews(){
        Log.d(TAG, "initViews: started");
        trainingName = findViewById(R.id.trainingName);
        trainingLongDesc = findViewById(R.id.trainingLongDesc);
        trainingImage = findViewById(R.id.trainingImage);

    }



}
