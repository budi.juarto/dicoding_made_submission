//package com.example.tabactivity;
//
//import android.util.Log;
//
//import java.util.ArrayList;
//
//public class Utils {
//    private static final String TAG ="Utils";
//
//    private static ArrayList<AnimeTraining> allTrainings;
//    private static ArrayList<AnimeTraining> movieList;
//
//    public Utils(){
//
//    }
//
//    public static String getTAG() {
//        return TAG;
//    }
//
//    public static ArrayList<AnimeTraining> getAllTrainings() {
//        return allTrainings;
//    }
//
//    public static ArrayList<AnimeTraining> getMovieList() {
//        return movieList;
//    }
//
//
//    public static void setAllTrainings(ArrayList<AnimeTraining> allTrainings) {
//        Utils.allTrainings = allTrainings;
//    }
//
//    public static void setMovieList(ArrayList<AnimeTraining> movieList) {
//        Utils.movieList= movieList;
//    }
//
//    public static void initializeAll(){
//        Log.d(TAG, "initializeAll: called");
//
//        if(null == allTrainings){
//            allTrainings = new ArrayList<>();
//        }
//
//        AnimeTraining movie1 = new AnimeTraining();
//        movie1.setName("からかい上手の高木さん2");
//        movie1.setShortDesc( "episode: 12");
//        movie1.setLongDesc("Second season of Karakai Jouzu no Takagi-san");
//        movie1.setImageUrl("https://cdn.myanimelist.net/images/anime/1792/101180l.jpg");
//        allTrainings.add(movie1);
//
//        AnimeTraining movie2 = new AnimeTraining();
//        movie2.setName("可愛ければ変態でも好きになってくれますか？");
//        movie2.setShortDesc("episode : 12");
//        movie2.setLongDesc("A high school student whose year of not having a girlfriend is his age, Keiki Kiryuu, suddenly received a love");
//        movie2.setImageUrl("https://cdn.myanimelist.net/images/anime/1601/99934l.jpg");
//        allTrainings.add(movie2);
//
//        AnimeTraining movie3 = new AnimeTraining();
//        movie3.setName("グランベルム");
//        movie3.setShortDesc("episode : 12");
//        movie3.setLongDesc("In a world that long ago featured the existence of magic, but has long since lost that ability. The story begins when the very normal high school student Mangetsu Kohinata meets Shingetsu Ernesta Fukami, who has migrated back to Japan from Germany, on a night with a full moon.");
//        movie3.setImageUrl("https://cdn.myanimelist.net/images/anime/1027/99942l.jpg");
//        allTrainings.add(movie3);
//
//        AnimeTraining movie4 = new AnimeTraining();
//        movie4.setName("ソウナンですか?");
//        movie4.setShortDesc("episode : 12");
//        movie4.setLongDesc("Because of a plane crash ... starting today, we're spending the springtime of our lives on a deserted island!! There's nothing here, so we have to make everything!! And eat everything!! (Ugh!) Check out our high-school-girl survival story of courage and knowledge. We're actually doing pretty well! We learn how to eat cicadas, how to build traps, a simple allergy test, how to eat hermit crabs, etc");
//        movie4.setImageUrl("https://cdn.myanimelist.net/images/anime/1325/99556l.jpg");
//        allTrainings.add(movie4);
//
//        AnimeTraining movie5 = new AnimeTraining();
//        movie5.setName("炎炎ノ消防隊");
//        movie5.setShortDesc("episode : 24");
//        movie5.setLongDesc("Year 198 of the Solar Era in Tokyo, special fire brigades are fighting against a phenomenon called spontaneous human combustion where humans beings are turned into living infernos called Infernals");
//        movie5.setImageUrl("https://cdn.myanimelist.net/images/anime/1967/100716l.jpg");
//        allTrainings.add(movie5);
//
//        AnimeTraining movie6 = new AnimeTraining();
//        movie6.setName("ナカノヒトゲノム【実況中");
//        movie6.setShortDesc("episode : 12");
//        movie6.setLongDesc("ride Akatsuki has unlocked hidden content in the game he's playing, Nakanohito Genome, and it turns out that this content is a real-life game");
//        movie6.setImageUrl("https://cdn.myanimelist.net/images/anime/1857/94908l.jpg");
//        allTrainings.add(movie6);
//
//        AnimeTraining movie7 = new AnimeTraining();
//        movie7.setName("荒ぶる季節の乙女どもよ。");
//        movie7.setShortDesc("episode : 12");
//        movie7.setLongDesc("The girls in a high school literature club do a little icebreaker to get to know each other answering the question, What's one thing you want to do before you die? One of the girls blurts out, Sex. Little do they know, the whirlwind unleashed by that word pushes each of these girls, with different backgrounds and personalities, onto their own clumsy, funny, painful, and emotional paths toward adulthood");
//        movie7.setImageUrl("https://cdn.myanimelist.net/images/anime/1883/96833l.jpg");
//        allTrainings.add(movie7);
//
//        AnimeTraining movie8 = new AnimeTraining();
//        movie8.setName("まちカドまぞく");
//        movie8.setShortDesc("episode : 12");
//        movie8.setLongDesc("Awakening her dormant abilities as a devil one day, Yuuko Yoshida aka Shadow Mistress Yuuko, is entrusted with the mission to defeat the Light clan's shrine maiden, a magical girl, by her ancestor Lilith. Yuuko meets magical girl Momo Chiyoda through her classmate Anri Sada, and challenges her to a duel, but loses quickly due to her lack of strength。");
//        movie8.setImageUrl("https://cdn.myanimelist.net/images/anime/1395/101109l.jpg");
//        allTrainings.add(movie8);
//
//        AnimeTraining movie9 = new AnimeTraining();
//        movie9.setName("女子高生の無駄づかい");
//        movie9.setShortDesc("episode : 12");
//        movie9.setLongDesc("One day out of boredom, Tanaka decided to give her classmates nicknames based on their quirks. Her friend Sakuchi became Ota for her nerdy interests, and her other friend Saginomiya became Robo because of her expressionless personality. In retaliation, her friends decided to name Tanaka Baka. These are the ridiculous days of three high school friends claiming (or not) the height of their youth.");
//        movie9.setImageUrl("https://cdn.myanimelist.net/images/anime/1004/95968l.jpg");
//        allTrainings.add(movie9);
//
//        AnimeTraining movie10 = new AnimeTraining();
//        movie10.setName("うちの娘の為ならば、俺はもしかしたら魔王も倒せるかもしれない");
//        movie10.setShortDesc("episode : 12");
//        movie10.setLongDesc("Dale is a cool, composed, and highly skilled adventurer who is made quite a name for himself despite his youth. One day on a job deep in the forest, he comes across a little devil girl who's almost wasted away. Unable to just leave her there to die, Dale takes her home and becomes her adoptive father");
//        movie10.setImageUrl("https://cdn.myanimelist.net/images/anime/1699/99227l.jpg");
//        allTrainings.add(movie10);
//
//
//    }
//
//    public static void initializeAll2(){
//
//        if(null == movieList){
//            movieList = new ArrayList<>();
//        }
//
//        AnimeTraining movie11 = new AnimeTraining();
//        movie11.setName("この音とまれ！");
//        movie11.setShortDesc( "episode: 12");
//        movie11.setLongDesc("Since the graduation of the senior members of the club, Takezou ends up being the sole member of the Koto (traditional Japanese string instrument) club. Now that the new school year has begun, Takezou will have to seek out new members into the club, or the club will become terminated. Out of nowhere, a new member barges into the near-abandoned club room, demanding to join the club. How will Takezou be able to keep his club alive and deal with this rascal of a new member?");
//        movie11.setImageUrl("https://cdn.myanimelist.net/images/anime/1464/99881l.jpg");
//        movieList.add(movie11);
//
//        AnimeTraining movie12 = new AnimeTraining();
//        movie12.setName("ひとりぼっちの○○生活");
//        movie12.setShortDesc("episode : 12");
//        movie12.setLongDesc("Hitori Bocchi suffers from extreme social anxiety, she's not good at talking to people, takes pretty extreme actions, is surprisingly adept at avoiding people, her legs cramp when she overexerts herself, gets full of herself when alone, will vomit when exposed to extreme tension and often comes up with plans. Now she is entering middle school and her only friend, Yawara Kai, is attending a different school. This leaves Bocchi alone, surrounded by new classmates with whom she must make friends before Kai will talk to her again.");
//        movie12.setImageUrl("https://cdn.myanimelist.net/images/anime/1130/99458l.jpg");
//        movieList.add(movie12);
//
//        AnimeTraining movie13 = new AnimeTraining();
//        movie13.setName("川柳少女");
//        movie13.setShortDesc("episode : 12");
//        movie13.setLongDesc("Yukishiro Nanako is a cute, cheerful high school girl with one peculiar trait—instead of verbal communication, she writes senryuu (a type of haiku) poems to relay her thoughts. Together with ex-delinquent Busujima Eiji, they are budding freshmen of the school's Literature Club. Even though Nanako doesn't talk, with the power of senryuu, the adorable pair has no problem enjoying their fun school-life through the tune of 5-7-5 syllables.");
//        movie13.setImageUrl("https://cdn.myanimelist.net/images/anime/1601/97030l.jpg");
//        movieList.add(movie13);
//
//        AnimeTraining movie14 = new AnimeTraining();
//        movie14.setName("鬼滅の刃");
//        movie14.setShortDesc("episode : 12");
//        movie14.setLongDesc(" Ever since the death of his father, the burden of supporting the family has fallen upon Tanjirou Kamado's shoulders. Though living impoverished on a remote mountain, the Kamado family are able to enjoy a relatively peaceful and happy life. One day, Tanjirou decides to go down to the local village to make a little money selling charcoal. On his way back, night falls, forcing Tanjirou to take shelter in the house of a strange man, who warns him of the existence of flesh-eating demons that lurk in the woods at night.");
//        movie14.setImageUrl("https://cdn.myanimelist.net/images/anime/1368/93482l.jpg");
//        movieList.add(movie14);
//
//        AnimeTraining movie15 = new AnimeTraining();
//        movie15.setName("賢者の孫");
//        movie15.setShortDesc("episode : 12");
//        movie15.setLongDesc("In the kingdom of Earlshide, Merlin Walford was once regarded as a national hero, hailed for both his power and achievements. Preferring a quiet life however, he secludes himself deep in the rural woods, dedicating his time to raising an orphan that he saved. This orphan is Shin, a normal salaryman in modern-day Japan who was reincarnated into Merlin's world while still retaining his past memories. As the years pass, Shin displays unparalleled talent in both magic casting and martial arts, much to Merlin's constant amazement.");
//        movie15.setImageUrl("https://cdn.myanimelist.net/images/anime/1398/94615l.jpg");
//        movieList.add(movie15);
//
//        AnimeTraining movie16 = new AnimeTraining();
//        movie16.setName("ぼくたちは勉強ができない");
//        movie16.setShortDesc("episode : 13");
//        movie16.setLongDesc("His late father always said that a useless man should strive to be useful, so to that end, third-year high school student Nariyuki Yuiga dedicated himself to becoming a high-achieving student in his school, despite his history of poor grades. In order to give his destitute family a better life, his ultimate goal is to obtain the special VIP nomination, a prestigious scholarship covering all future university tuition fees. Although Nariyuki could feasibly be a shoe-in for the nomination, he is constantly overshadowed by classmates Rizu Ogata and Fumino Furuhashi in mathematics and literature, respectively.");
//        movie16.setImageUrl("https://cdn.myanimelist.net/images/anime/1602/100510l.jpg");
//        movieList.add(movie16);
//
//        AnimeTraining movie17 = new AnimeTraining();
//        movie17.setName("文豪ストレイドッグス 第3期。");
//        movie17.setShortDesc("episode : 12");
//        movie17.setLongDesc("Following the conclusion of the three-way organizational war, government bureaucrat Ango Sakaguchi recalls an event that transpired years ago, after the death of the former Port Mafia boss. Osamu Dazai, still a new recruit at the time, was tasked with investigating rumors related to a mysterious explosion that decimated part of the city years ago—and its connection to the alleged reappearance of the former boss.");
//        movie17.setImageUrl("https://cdn.myanimelist.net/images/anime/1759/100578l.jpg");
//        movieList.add(movie17);
//
//        AnimeTraining movie18 = new AnimeTraining();
//        movie18.setName("なんでここに先生が!?");
//        movie18.setShortDesc("episode : 12");
//        movie18.setLongDesc("7-year-old Ichirou Satou is an average teenager who always happens to find himself in perverted situations with his teacher, Kana Kojima. Follow this erotic love comedy about their mishaps throughout their daily lives and how Ichirou and Kana choose to handle them.");
//        movie18.setImageUrl("https://cdn.myanimelist.net/images/anime/1527/95171l.jpg");
//        movieList.add(movie18);
//
//        AnimeTraining movie19 = new AnimeTraining();
//        movie19.setName("進撃の巨人");
//        movie19.setShortDesc("episode : 12");
//        movie19.setLongDesc("Seeking to restore humanity’s diminishing hope, the Survey Corps embark on a mission to retake Wall Maria, where the battle against the merciless \"Titans\" takes the stage once again.\n" +
//                "\n" +                "Returning to the tattered Shiganshina District that was once his home, Eren Yeager and the Corps find the town oddly unoccupied by Titans. Even after the outer gate is plugged, they strangely encounter no opposition. The mission progresses smoothly until Armin Arlert, highly suspicious of the enemy's absence, discovers distressing signs of a potential scheme against them.\n");
//        movie19.setImageUrl("https://cdn.myanimelist.net/images/anime/1159/95649l.jpg");
//        movieList.add(movie19);
//
//        AnimeTraining movie20 = new AnimeTraining();
//        movie20.setName("フルーツバスケット");
//        movie20.setShortDesc("episode : 12");
//        movie20.setLongDesc("Tooru Honda has always been fascinated by the story of the Chinese Zodiac that her beloved mother told her as a child. However, a sudden family tragedy changes her life, and subsequent circumstances leave her all alone. Tooru is now forced to live in a tent, but little does she know that her temporary home resides on the private property of the esteemed Souma family. Stumbling upon their home one day, she encounters Shigure, an older Souma cousin, and Yuki, the \"prince\" of her school. Tooru explains that she lives nearby, but the Soumas eventually discover her well-kept secret of being homeless when they see her walking back to her tent one night.");
//        movie20.setImageUrl("https://cdn.myanimelist.net/images/anime/1937/96524l.jpg");
//        movieList.add(movie20);
//    }
//
//}
